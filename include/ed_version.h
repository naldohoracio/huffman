/**!
 * @headerfile version.h
 * @author Edvonaldo Horácio (edvonaldohs@ic.ufal.br)
 * @brief
 * @version 0.1
 * @date 2019-06-14
 *
 * @copyright Copyright (c) IC 2018
 *
 */
#ifndef _ED_VERSION_H_
#define _ED_VERSION_H_

#include <stdio.h>
#include <stdlib.h>
#include "number_version.h"

typedef struct
{
    u_int8_t major;
    u_int8_t minor;
    u_int16_t build;
}version_t;

const version_t *version_instance();

u_int8_t version_get_major();

u_int8_t version_get_minor();

u_int16_t version_get_build();

#endif // _ED_VERSION_H