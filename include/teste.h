/**!
 * @headerfile teste.h
 * @author Edvonaldo Horácio (edvonaldohs@ic.ufal.br)
 * @brief
 * @version 0.1
 * @date 2019-06-11
 *
 * @copyright Copyright (c) IC 2018
 *
 */
#ifndef _TESTE_H_
#define _TESTE_H_

#include <stdio.h>
#include <stdlib.h>

void my_print();

#endif // _TESTE_H_