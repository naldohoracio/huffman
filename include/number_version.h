/**!
 * @headerfile number_version.h
 * @author Edvonaldo Horácio (edvonaldohs@ic.ufal.br)
 * @brief
 * @version 0.1
 * @date 2019-06-14
 *
 * @copyright Copyright (c) IC 2018
 *
 */
/* File was auto-generated, do not modify! */

#ifndef _NUMBER_VERSION_H_
#define _NUMBER_VERSION_H_

#define VERSION_MAJOR 0
#define VERSION_MINOR 1
#define VERSION_BUILD 0

#endif // _NUMBER_VERSION_H_