/**!
 * @headerfile teste.h
 * @author Edvonaldo Horácio (edvonaldohs@ic.ufal.br)
 * @brief
 * @version 0.1
 * @date 2019-06-11
 *
 * @copyright Copyright (c) IC 2018
 *
 */
#include "teste.h"

void my_print()
{
    printf("Hello world!\n");
}