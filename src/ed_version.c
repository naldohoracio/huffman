/**!
 * @headerfile version.h
 * @author Edvonaldo Horácio (edvonaldohs@ic.ufal.br)
 * @brief
 * @version 0.1
 * @date 2019-06-14
 *
 * @copyright Copyright (c) IC 2018
 *
 */
#include "ed_version.h"

static const version_t __version = {VERSION_MAJOR, VERSION_MINOR, VERSION_BUILD};

const version_t *version_instance()
{
    return &__version;
}

u_int8_t version_get_major()
{
    return __version.major;
}

u_int8_t version_get_minor()
{
    return __version.minor;
}
u_int16_t version_get_build()
{
    return __version.build;
}