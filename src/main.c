/**!
 * @headerfile main.c
 * @author Edvonaldo Horácio (edvonaldohs@ic.ufal.br)
 * @brief
 * @version 0.1
 * @date 2019-06-11
 *
 * @copyright Copyright (c) IC 2018
 *
 */

#include "teste.h"
#include "ed_version.h"

int main() {
    my_print();
    return 0;
}