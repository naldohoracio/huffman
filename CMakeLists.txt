cmake_minimum_required(VERSION 3.13)
project(huffman C)

set(CMAKE_C_STANDARD 99)

# Generate version file
include(build.version.cmake NO_POLICY_SCOPE)

add_custom_command(
        TARGET include
        POST_BUILD
        COMMAND "${CMAKE_COMMAND}"
        -DPROJECT_SOURCE_DIR=${PROJECT_SOURCE_DIR}
        -DPROJECT_BINARY_DIR=${PROJECT_BINARY_DIR}
        -P ${PROJECT_SOURCE_DIR}/build.version.cmakes)

# add the binary tree to the search path for include files
# so that we will find TutorialConfig.h
include_directories("${PROJECT_BINARY_DIR}")

add_executable(huffman
        src/main.c
        src/teste.c
        src/ed_version.c
        )

include_directories(include)